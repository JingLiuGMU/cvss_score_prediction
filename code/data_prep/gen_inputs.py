import pandas as pd
import datetime
import numpy as np
import json
import argparse, os
from scipy import sparse
from sklearn.metrics.pairwise import cosine_similarity


def load_data(filename, dir, cve_df, month):
    df = pd.read_pickle(filename)
    selected_df = cve_df.merge(df, on='cve', how='left')
    dicts = args.dir + 'cve_id_map.txt'
    if selected_df.isnull().values.any():
        raise ValueError('not all cves in {} included total data!!'.format(month))
    else:
        print('column names after merge: {}'.format(list(selected_df)))
    selected_df = id_map(selected_df, 'cve', dicts, dir=dir)
    for column in ('cvss_av', 'cvss_ac', 'cvss_pr', 'cvss_ui','cvss_s', 'cvss_c', 'cvss_i', 'cvss_a'):
        selected_df = id_map(selected_df, column, dir=dir)
        # print(selected_df.shape)

    selected_df = selected_df.sort_values(by=['cve_id'])
    print('selected_df head: {}'.format(selected_df.head()))
    return selected_df


def id_map(df, column, dicts=None, dir=None):
    if not dicts:
        unique_value = pd.unique(df[column].values.ravel('K'))
        ids = dict([(y, x) for x, y in enumerate(sorted(unique_value))])
        with open(dir + column + '_id_map.txt', 'w') as file:
            file.write(str(ids))
    else:
        ids = eval(open(dicts, 'r').read())

    df.insert(column=column + '_id', value=df[column].map(ids), loc=0)
    df = df.dropna()
    if column != "cve":
        df = df.drop([column], axis=1)
    return df


def gen_labels(args):
    cves_df = get_cve_each_period(args)
    inputs_df = load_data(args.filename, args.dir, cves_df, args.end_time)
    print('{} CVEs released for month {}'.format(inputs_df.shape[0], args.end_time))

    inputs_df['mask'] = np.where(inputs_df['split'] == 'test', 0, 1)

    test_cves = inputs_df[inputs_df['split']=='test']
    test_cves[['cve', 'cve_id']].to_pickle(args.dir + 'test_cves.pkl')

    print('{} out of {} cves has labels'.format(inputs_df['mask'].sum(), inputs_df.shape[0]))

    inputs_df = inputs_df.sort_values(by=['cve_id'])
    print('sorted inputs_df: {}'.format(inputs_df.head()))

    print(list(inputs_df))

    label_df = inputs_df[['cve_id', 'cvss_av_id', 'cvss_ac_id', 'cvss_pr_id', 'cvss_ui_id',
                          'cvss_s_id', 'cvss_c_id', 'cvss_i_id', 'cvss_a_id', 'cvss_score', 'mask']]
    # print('@@@@@@@@@@@@@@no. of cves {}'.format(label_df.shape[0]))
    np.save(args.dir + 'labels.npy', label_df.values)


def gen_featuers(args):
    features = pd.read_pickle(args.features)
    dicts = args.dir + 'cve_id_map.txt'
    features = id_map(features, 'cve', dicts)
    features = features.sort_values(by=['cve_id'])
    features_df = features[['cve_id', 'num_tweets', 'sum_followers','sum_followees', 'sum_retweets',
                            'sum_favroited', 'avg_urls', 'avg_hashtags', 'avg_user_mentions', 'num_verified',
                            'avg_tweet_count', 'avg_user_age']]
    print(features_df.head())
    if args.pca:
        more_feat = attach_pca(args.pca_filename, dicts)
        filename = 'thres_' + str(args.threshold) + '_pca_' + \
                   args.pca_filename.split('/')[-1].split('.')[0].split('-')[-1] + '_features.npy'
        print('pca feature name {}'.format(args.dir + filename))
    elif args.darkemb:
        more_feat = attach_darkemb(features_df, dicts)
        filename = 'features.npy'
    else:
        more_feat = None
        filename = 'features.npy'
    if more_feat is not None:
        features_array = np.hstack((features_df.values, more_feat))
    else:
        features_array = features_df.values
    np.save(args.dir + filename, features_array)


def attach_darkemb(df, dicts):
    darkemb_feat_dict = pd.read_pickle('cvss_data.pkl')
    darkemb_feat = []
    ids = eval(open(dicts, 'r').read())
    for key, item in ids.items():
        temp_darkemb_feat = darkemb_feat_dict[key]['vector_5_1']
        temp_darkemb_feat = np.hstack((item, temp_darkemb_feat))
        darkemb_feat.append(temp_darkemb_feat)
    print('len of darkemb_feat: {}'.format(len(darkemb_feat)))
    darkemb_feat = np.vstack(darkemb_feat)
    print('dark feat dim {}'.format(darkemb_feat.shape[0]))
    print('df dim {}'.format(df.shape[0]))
    assert darkemb_feat.shape[0] == df.shape[0]
    assert (darkemb_feat[:, 0] == df['cve_id'].values).all()
    return darkemb_feat[:, 1:]


def attach_pca(filename, dicts):
    # import pickle
    # with open(filename, 'rb') as fin:
    #     pca = pickle.load(fin)
    pca = pd.read_pickle(filename)
    ids = eval(open(dicts, 'r').read())
    pca.insert(column='cve_id', value=pca.index.map(ids), loc=0)
    # df[column + '_id'] = df[column].map(ids)
    pca = pca.dropna()
    pca = pca.sort_values(by=['cve_id'])
    pca = pca.drop(['cve_id'], axis=1)
    return pca.values


def gen_adj_m(args):
    dicts = args.dir + 'cve_id_map.txt'
    pca_feat = attach_pca(args.pca_filename, dicts)
    cos_simi = cosine_similarity(pca_feat)

    adj_m = (cos_simi >= args.threshold).astype(int)
    adj_m -= np.eye(adj_m.shape[0]).astype(int)
    print('mean degree: {}'.format(np.mean(adj_m.sum(axis=0))))
    print('adj shape: {}'.format(adj_m.shape))
    np.save(args.dir + 'adj_m_thres_' + str(args.threshold) + ".npy", adj_m)


def get_text_infor(args):
    cve_sentences_df = pd.read_pickle(args.sentences_file)
    dicts = args.dir + 'cve_id_map.txt'
    cve_sentences_df = id_map(cve_sentences_df, 'cve', dicts)
    print(cve_sentences_df.head())
    cve_sentences_df.to_pickle(args.dir + 'sentences.pkl')


def get_cve_each_period(args):
    cat_network = pd.read_pickle(args.cat_graph)

    label_mask = pd.DataFrame.from_records([(i, cat_network['cve_graph'][i]['split'])
                                            for i in cat_network['cve_graph'].keys()], columns=['cve', 'split'])
    print('row count before remove unknown {}'.format(label_mask.shape[0]))
    label_mask = label_mask[label_mask['split'] != 'unknown']
    print('row count after remove unknown {}'.format(label_mask.shape[0]))
    print(label_mask.head())
    unique_value = pd.unique(label_mask['cve'].values.ravel('K'))
    ids = dict([(y, x) for x, y in enumerate(sorted(unique_value))])
    with open(args.dir + 'cve_id_map.txt', 'w') as file:
        file.write(str(ids))

    label_mask.to_pickle(args.dir + 'label_mask.pkl')
    return label_mask


if __name__ == "__main__":
    parser = argparse.ArgumentParser(description='input_generation')
    parser.add_argument('--filename', '-f', type=str, default='cve_ground_truth.pkl')
    parser.add_argument('--end_time', '-e', type=str, default='2017-07')
    parser.add_argument('--features', '-feat', type=str, default='cve_features.pkl')
    parser.add_argument('--cat_graph', '-g', type=str, default='10-0.pkl')
    parser.add_argument('--dir', '-od', type=str, default='./data/10_days/')
    parser.add_argument('--pca_filename', '-file', type=str, default='pca/10-0-300.pkl')
    parser.add_argument('--sentences_file', '-sf', type=str, default='cve_sentences.pkl')
    parser.add_argument('--pca', '-p', type=bool, default=False)
    parser.add_argument('--darkemb', '-d', type=bool, default=False)
    parser.add_argument('--threshold', '-th', type=float, default=0.3)

    args = parser.parse_args()
    args.filename = args.dir + args.filename
    args.features = args.dir + args.features
    args.cat_graph = args.dir + args.cat_graph
    args.pca_filename = args.dir + args.pca_filename
    args.sentences_file = args.dir + args.sentences_file
    args.dir += args.end_time + '/'
    if not os.path.exists(args.dir):
        os.makedirs(args.dir)
    # get_cve_each_period(args)
    #gen_labels(args)
    gen_featuers(args)
    #gen_adj_m(args)
    #get_text_infor(args)



