import networkx as nx
import pandas as pd
import numpy as np
import matplotlib.pyplot as plt


def visualize_graph():
    adj = np.load('data/2017_Jan/adj_m.npy')
    #G = nx.from_pandas_edgelist(data, 's_user', 'd_user', create_using=nx.Graph())
    G = nx.from_numpy_matrix(adj)
    pos=nx.spring_layout(G)
    nx.draw_networkx_nodes(G, pos, node_shape='o', node_size=10)
    nx.draw_networkx_edges(G, pos)

    plt.show()


def cal_label_distribution(labels, num_metrics):
    for i in range(1, num_metrics + 1):
        label = labels[:, i].astype(int)
        #print('{} labels for metrics {}'.format(np.max(label) + 1, i))
        print('the number of label {}: {}'.format(i, np.bincount(label)))


def plot_hist_cvss_score(labels):
    label = labels[:, -2]
    plt.hist(label, bins='auto')
    plt.title("Histogram of cvss score")
    plt.show()


def plot_bar_chart(data, metric_id, dir):
    label = data[:, metric_id + 1].astype(int)
    train_mask = data[:, -1]
    train_mask = train_mask.astype(int)
    train_label = label[train_mask == 1]
    test_label = label[train_mask == 0]
    train_dis = np.bincount(train_label)
    train_index = np.arange(len(train_dis))

    test_dis = np.bincount(test_label)
    test_index = np.arange(len(test_dis))
    plt.subplot(121)
    plt.bar(train_index, train_dis)
    plt.title('label distribution for train, metric ' + str(metric_id))
    plt.subplot(122)
    plt.bar(test_index, test_dis)
    plt.title('label distribution for test, metric ' + str(metric_id))
    plt.savefig(dir + 'label_distribution_metric ' + str(metric_id) + '.png')
    plt.clf()


if __name__ == "__main__":
    cvss_labels = np.load('data/10_days/2017-07/labels.npy')
    #cvss_labels = np.load('data/all_data/labels.npy')
    # for i in range(8):
    #     plot_bar_chart(cvss_labels, i, 'data/10_days/2017-07/')
    cal_label_distribution(cvss_labels, 8)
    plot_hist_cvss_score(cvss_labels)





