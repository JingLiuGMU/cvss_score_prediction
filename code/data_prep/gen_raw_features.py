import numpy as np
import pickle
import pandas as pd
import networkx as nx
import datetime
import calendar
from dateutil.relativedelta import relativedelta
import argparse, os

'''For current dataset, there are 31971 CVEs. 
    min date: 2016-07-01
    max date: 2018-12-07'''


def process_data(args):
    cvss_data = pd.read_pickle(args.input)
    cvss_df = pd.DataFrame.from_records([(i, cvss_data[i]['num_tweets'], cvss_data[i]['pub_date'],
                                          cvss_data[i]['date_diff_mod_pub'], cvss_data[i]['date_diff_now_mod'],
                                          cvss_data[i]['cvss_av'], cvss_data[i]['cvss_ac'],
                                          cvss_data[i]['cvss_pr'], cvss_data[i]['cvss_ui'], cvss_data[i]['cvss_s'],
                                          cvss_data[i]['cvss_c'], cvss_data[i]['cvss_i'], cvss_data[i]['cvss_a'],
                                          cvss_data[i]['cvss_score'])
                                         for i in cvss_data.keys()],
                                        columns=['cve', 'num_tweets', 'pub_date','date_diff_mod_pub',
                                                 'date_diff_now_mod', 'cvss_av', 'cvss_ac', 'cvss_pr', 'cvss_ui',
                                                 'cvss_s', 'cvss_c', 'cvss_i', 'cvss_a', 'cvss_score'])
    print('{} CVEs before dropping those with zero tweets'.format(cvss_df.shape[0]))
    cvss_df = cvss_df[cvss_df['num_tweets'] != 0]
    print('{} CVEs after dropping those with zero tweets'.format(cvss_df.shape[0]))
    cvss_df = cvss_df.assign(time=lambda x: pd.to_datetime(x['pub_date']))
    cvss_df['cvss_pub_date'] = cvss_df['time'] + pd.to_timedelta(cvss_df['date_diff_mod_pub'], unit='d')
    cvss_df['pub_date'] = cvss_df['time'].apply(lambda x: x.strftime("%Y-%m-%d"))
    cvss_df['cvss_pub_date'] = cvss_df['cvss_pub_date'].apply(lambda x: x.strftime("%Y-%m-%d"))

    print('cve_ground_truth shape after merge: {}'.format(cvss_df.shape))

    print(cvss_df.head())
    if not os.path.exists(args.output):
        os.makedirs(args.output)
    cvss_df.to_pickle(args.output + 'cve_ground_truth.pkl')


def add_month(now):
    try:
        then = (now + relativedelta(months=1)).replace(day=now.day)
    except ValueError:
        then = (now + relativedelta(months=2)).replace(day=1)
    return then


def check_data():
    cve = pd.read_pickle(args.output + 'cve_ground_truth.pkl')
    min_date = cve.pub_date.min()
    max_date = cve.pub_date.max()
    print('min date: {}, max date: {}'.format(min_date, max_date))

    # current_date = add_month(datetime.datetime.strptime(min_date,"%Y-%m-%d"))
    min_date = datetime.datetime.strptime('2017-01-01', "%Y-%m-%d")
    max_date = datetime.datetime.strptime(max_date, "%Y-%m-%d")
    current_date = add_month(min_date)
    print('current_time: {}'.format(current_date))
    cve['pub_date'] = cve['pub_date'].apply(lambda x: datetime.datetime.strptime(x, "%Y-%m-%d"))
    df = pd.DataFrame(columns=['from', 'to', 'number of cve'])
    while current_date < max_date:
        sub_df = cve[(cve['pub_date'] >= min_date) & (cve['pub_date'] < current_date)]
        print('# of CVEs between {} and {} is {}'.format(min_date, current_date, sub_df.shape[0]))
        s = pd.Series([min_date, current_date, sub_df.shape[0]], index=['from', 'to', 'number of cve'])
        df = df.append(s, ignore_index=True)
        min_date = current_date
        current_date = add_month(min_date)
    df.to_csv(args.output + 'cve_distribution_over_time.csv', index=False)


def gen_cve_features(args):
    cvss_data = pd.read_pickle(args.input)
    features = ['cve', 'num_tweets', 'sum_followers','sum_followees', 'sum_retweets', 'sum_favroited', 'avg_urls',
                'avg_hashtags', 'avg_user_mentions', 'num_verified', 'avg_tweet_count', 'avg_user_age']
    cvss_df = pd.DataFrame.from_records([(i, cvss_data[i]['num_tweets'], cvss_data[i]['sum_followers'],
                                          cvss_data[i]['sum_followees'], cvss_data[i]['sum_retweets'],
                                          cvss_data[i]['sum_favroited'], cvss_data[i]['avg_urls'],
                                          cvss_data[i]['avg_hashtags'], cvss_data[i]['avg_user_mentions'],
                                          cvss_data[i]['num_verified'], cvss_data[i]['avg_tweet_count'],
                                          cvss_data[i]['avg_user_age'])
                                         for i in cvss_data.keys()],
                                        columns=features)
    print('cvss feature df shape: {}'.format(cvss_df.shape))

    print(cvss_df.head())
    cvss_df.to_pickle(args.output + 'cve_features.pkl')


def get_cve_sentences():
    cvss_data = pd.read_pickle(args.input)
    columns = ['cve', 'text', 'num_tweets']
    text_df = pd.DataFrame.from_records([(i, cvss_data[i]['text'], cvss_data[i]['num_tweets']) for i in cvss_data.keys()
                                         if cvss_data[i]['num_tweets'] > 0], columns=columns)
    print('cvss sentences df shape: {}'.format(text_df.shape))

    print(text_df.head())
    text_df.to_pickle(args.output + 'cve_sentences.pkl')


if __name__ == '__main__':
    parse = argparse.ArgumentParser()
    parse.add_argument('--input', '-i', type=str, help='pickle file containing raw features',
                       default='./data/20_days/cvss_data.pkl')
    # parse.add_argument('--cat_graph', '-g', type=str, help='pickle file containing cat graph',
    #                    default='./data/cat_network_weight.pkl')
    parse.add_argument('--output', '-o', type=str, default='./data/20_days/')
    args = parse.parse_args()
    process_data(args)
    gen_cve_features(args)
    check_data()
    get_cve_sentences()
