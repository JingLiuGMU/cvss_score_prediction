from gensim.test.utils import common_texts
from gensim.models.doc2vec import Doc2Vec, TaggedDocument
import numpy as np
import pickle
import pandas as pd

cvss = pd.read_pickle('data/cvss_data.pkl')

documents = []

for i, doc in cvss.items():
    if doc['num_tweets'] != 0:
        for t in doc['text'].split('\n'):
            documents.append(TaggedDocument(t, [i]))

#for window in (5, 7, 10):
window = 5
model = Doc2Vec(documents, vector_size=101, window=window, min_count=1, workers=4)

for i, doc in cvss.items():
    if doc['num_tweets'] != 0:
        key = 'vector_' + str(window) + '_1'
        cvss[i][key] = model.docvecs[i]
            #print(cvss[i]['vector'])

pd.to_pickle(cvss, 'cvss_data.pkl')
#
# np.savez('pvdm.npz', key=model.docvecs.
# model.docvecs.vectors_docs)
