import os
import sys
import json
import numpy as np
import pickle as pkl
import networkx as nx
import scipy.sparse as sp
import matplotlib.pyplot as plt
import itertools
from scipy.sparse.linalg.eigen.arpack import eigsh
from networkx.readwrite import json_graph
from sklearn.metrics import f1_score, recall_score, precision_score, accuracy_score, roc_auc_score
from sklearn.metrics import r2_score, mean_squared_error, mean_absolute_error, confusion_matrix


def parse_index_file(filename):
    """Parse index file."""
    index = []
    for line in open(filename):
        index.append(int(line.strip()))
    return index


def sample_mask(idx, l):
    """Create mask."""
    mask = np.zeros(l)
    mask[idx] = 1
    return np.array(mask, dtype=np.bool)


def load_data(dataset_str):
    return load_small_data(dataset_str)


def load_seq_data(dataset_prefix):
    with open(dataset_prefix + 'tweet_seq.pkl', 'rb') as handle:
        seq = pkl.load(handle)


    with open(dataset_prefix + 'word_dict.pkl', 'rb') as fin:
        word_dict = pkl.load(fin)

    print('vocab size: {}'.format(len(word_dict) + 1))
    return seq, len(word_dict) + 1


def load_cvss_data(dataset_prefix, metrics_id=0, reg=False, feat_type='basic', threshold=0.5):
    adj = np.load(dataset_prefix + 'adj_m_thres_' + str(threshold) + '.npy')
    mean_deg = np.mean(adj.sum(axis=0))
    max_deg = np.max(adj.sum(axis=0))
    print('mean degree {}, max degree {}'.format(mean_deg, max_deg))
    if feat_type == 'basic':
        features = np.load(dataset_prefix + 'features.npy')[:, 1:12]
    elif feat_type == 'DarkEmb':
        features = np.load(dataset_prefix + 'features.npy')[:, 1:]
    elif feat_type == 'PCA':
        features = np.load(dataset_prefix + 'thres_' + str(threshold) + '_pca_300_features.npy')[:, 1:]

    label_and_mask = np.load(dataset_prefix + 'labels.npy')
    train_mask = label_and_mask[:, -1]
    train_mask = train_mask.astype(int)
    print('# of training cves {}'.format(np.count_nonzero(train_mask)))
    if not reg:
        assert metrics_id < 8
        labels = label_and_mask[:, metrics_id + 1]
        labels = labels.astype(int)
        train_label = labels[train_mask==1]
        weights = 1 - np.bincount(train_label) / len(train_label)
        print('weights for training data {}'.format(weights))
        labels = one_hot_encoding(labels)
    else:
        labels = label_and_mask[:, -2]
        labels /= 10
        print('max of labels: {}'.format(np.max(labels)))
        weights = 0
    val_test_mask = 1 - train_mask
    nonzero_idx = np.nonzero(val_test_mask)[0]
    np.random.seed(42)
    val_mask_idx = np.random.choice(nonzero_idx, size=int(0.2 * len(nonzero_idx)), replace=False)
    print('# of val cves {}'.format(len(val_mask_idx)))
    val_test_mask[val_mask_idx] = 0
    print('# of test cves {}'.format(np.count_nonzero(1 - train_mask)))
    val_mask = 1 - train_mask - val_test_mask
    return adj, features, labels, train_mask, val_mask, 1 - train_mask, weights


def one_hot_encoding(a):
    n_values = np.max(a) + 1
    return np.eye(n_values)[a]


def load_small_data(dataset_str):
    """Load data."""
    names = ['x', 'y', 'tx', 'ty', 'allx', 'ally', 'graph']
    objects = []
    for i in range(len(names)):
        with open("data/ind.{}.{}".format(dataset_str, names[i]), 'rb') as f:
            if sys.version_info > (3, 0):
                objects.append(pkl.load(f, encoding='latin1'))
            else:
                objects.append(pkl.load(f))

    x, y, tx, ty, allx, ally, graph = tuple(objects)
    test_idx_reorder = parse_index_file("data/ind.{}.test.index".format(dataset_str))
    test_idx_range = np.sort(test_idx_reorder)

    features = sp.vstack((allx, tx)).tolil()
    features[test_idx_reorder, :] = features[test_idx_range, :]
    adj = nx.adjacency_matrix(nx.from_dict_of_lists(graph))

    labels = np.vstack((ally, ty))
    labels[test_idx_reorder, :] = labels[test_idx_range, :]

    idx_test = test_idx_range.tolist()
    idx_train = range(len(y))
    idx_val = range(len(y), len(y)+500)

    train_mask = sample_mask(idx_train, labels.shape[0])
    val_mask = sample_mask(idx_val, labels.shape[0])
    test_mask = sample_mask(idx_test, labels.shape[0])

    y_train = np.zeros(labels.shape)
    y_val = np.zeros(labels.shape)
    y_test = np.zeros(labels.shape)
    y_train[train_mask, :] = labels[train_mask, :]
    y_val[val_mask, :] = labels[val_mask, :]
    y_test[test_mask, :] = labels[test_mask, :]

    return adj, features, y_train, y_val, y_test, train_mask, val_mask, test_mask


def sparse_to_tuple(sparse_mx):
    """Convert sparse matrix to tuple representation."""
    def to_tuple(mx):
        if not sp.isspmatrix_coo(mx):
            mx = mx.tocoo()
        coords = np.vstack((mx.row, mx.col)).transpose()
        values = mx.data
        shape = mx.shape
        return coords, values, shape

    if isinstance(sparse_mx, list):
        for i in range(len(sparse_mx)):
            sparse_mx[i] = to_tuple(sparse_mx[i])
    else:
        sparse_mx = to_tuple(sparse_mx)

    return sparse_mx


def preprocess_features(features, sparse=True):
    """Row-normalize feature matrix and convert to tuple representation"""
    colsum = np.array(features.sum(0))
    #import ipdb; ipdb.set_trace()
    try:
        r_inv = np.power(colsum, -1).flatten()
        r_inv[np.isinf(r_inv)] = 0.
        #r_mat_inv = sp.diags(r_inv)
        r_mat_inv = sp.csr_matrix(r_inv)
        features = r_mat_inv.T.multiply(features.T).T.todense()
    except Exception as e:
        print(e)
    
    if sparse:
        return sparse_to_tuple(features)
    return features #features.todense()


def normalize_adj(adj):
    """Symmetrically normalize adjacency matrix."""
    adj = sp.coo_matrix(adj)
    rowsum = np.array(adj.sum(1))
    d_inv_sqrt = np.power(rowsum, -0.5).flatten()
    d_inv_sqrt[np.isinf(d_inv_sqrt)] = 0.
    d_mat_inv_sqrt = sp.diags(d_inv_sqrt)
    # return adj.dot(d_mat_inv_sqrt).transpose().dot(d_mat_inv_sqrt).tocoo()
    return d_mat_inv_sqrt.dot(adj).dot(d_mat_inv_sqrt).tocoo()


def preprocess_adj(adj, norm=True, sparse=False):
    """Preprocessing of adjacency matrix for simple GCN model and conversion to tuple representation."""
    # adj = adj + sp.eye(adj.shape[0])
    if norm:
        adj = normalize_adj(adj)
    if sparse:
        return sparse_to_tuple(adj)
    return adj.todense()


def construct_feed_dict(features, support, labels, labels_mask, is_training, placeholders):
    """Construct feed dictionary."""
    feed_dict = {
        placeholders['labels']: labels,
        placeholders['labels_mask']: labels_mask,
        placeholders['features']: features,
        placeholders['support']: support,
        placeholders['num_features_nonzero']: features.shape,
        placeholders['is_training']: is_training}
    return feed_dict


def calc_f1(y_true, y_pred, mask):
    y_true = np.argmax(y_true, axis=1)
    y_pred = np.argmax(y_pred, axis=1)
    mask = mask.astype(bool)
    y_true = np.squeeze(y_true[mask])
    y_pred = np.squeeze(y_pred[mask])
    # print('y true shape {}, y pred shape {}'.format(y_true.shape, y_pred.shape))
    # np.savetxt("pred_true_test.csv", np.c_[y_pred, y_true], delimiter=",")
    mac_recall = recall_score(y_true, y_pred, average='macro')
    mac_prec = precision_score(y_true, y_pred, average='macro')
    mic_recall = recall_score(y_true, y_pred, average='weighted')
    mic_prec = precision_score(y_true, y_pred, average='weighted')

    # avg_prec = average_precision_score(y_true, y_pred, average='micro')
    mic = f1_score(y_true, y_pred, average="weighted")
    mac = f1_score(y_true, y_pred, average="macro")
    # accuracy = accuracy_score(y_true, y_pred)
    f1 = f1_score(y_true, y_pred, average="micro")
    return mac_recall, mac_prec, mic_recall, mic_prec, mic, mac, f1, y_pred, y_true


def cal_auc(y_true, y_pred, mask):
    #y_true = np.argmax(y_true, axis=1)
    #y_pred = np.argmax(y_pred, axis=1)
    mask = mask.astype(bool)
    #y_true = np.squeeze(y_true[mask])
    #y_pred = np.squeeze(y_pred[mask])
    y_true = y_true[mask]
    y_score = y_pred[mask]
    auc = roc_auc_score(y_true=y_true, y_score=y_score, average='weighted')
    return auc


def cal_evl_metric(pred, label, mask, remove=False):
    print('pred shape: {}'.format(pred.shape))
    print('label shape: {}'.format(label.shape))
    pred = np.squeeze(pred, axis=1)
    print('pred max: {}'.format(np.max(pred)))
    print('label max: {}'.format(np.max(label)))
    if remove:
        pred = pred[np.where(pred > 0.)]
        label = label[np.where(pred > 0.)]
    mask = mask.astype(bool)
    print(mask)
    label = np.squeeze(label[mask])
    pred = np.squeeze(pred[mask])
    print('pred max: {}'.format(np.max(pred)))
    print('label max: {}'.format(np.max(label)))
    r2 = r2_score(label, pred)
    mae = mean_absolute_error(label, pred)
    mse = mean_squared_error(label, pred)
    mape = mean_absolute_percentage_error(label, pred)
    np.savetxt("pred_true_test.csv", np.c_[pred, label], delimiter=",")
    print('metrics: r2 {}, mae {}, mase {}, mape {}'.format(r2, mae, mse, mape))
    return r2, mae, mse, mape, label, pred


def mean_absolute_percentage_error(y_true, y_pred):
    predi = y_pred[np.where(y_true > 1e-5)]
    labeli = y_true[np.where(y_true > 1e-5)]
    return np.mean(np.abs(labeli - predi) / labeli)


def save_confusion_matrix(y_true, y_pred, num_class, title, dir):
    cnf_matrix = confusion_matrix(y_true, y_pred)
    np.set_printoptions(precision=2)
    plt.figure()
    class_names = np.arange(num_class)
    plot_confusion_matrix(cnf_matrix, classes=class_names,
                          title='Confusion matrix, ' + title)
    plt.savefig(dir + title + '_confusion_matrix.png')


def plot_confusion_matrix(cm, classes,
                          normalize=False,
                          title='Confusion matrix',
                          cmap=plt.cm.Blues):
    """
    This function prints and plots the confusion matrix.
    Normalization can be applied by setting `normalize=True`.
    """
    if normalize:
        cm = cm.astype('float') / cm.sum(axis=1)[:, np.newaxis]
        print("Normalized confusion matrix")
    else:
        print('Confusion matrix, without normalization')

    print(cm)

    plt.imshow(cm, interpolation='nearest', cmap=cmap)
    plt.title(title)
    plt.colorbar()
    tick_marks = np.arange(len(classes))
    plt.xticks(tick_marks, classes)
    plt.yticks(tick_marks, classes)

    fmt = '.2f' if normalize else 'd'
    thresh = cm.max() / 2.
    for i, j in itertools.product(range(cm.shape[0]), range(cm.shape[1])):
        plt.text(j, i, format(cm[i, j], fmt),
                 horizontalalignment="center",
                 color="white" if cm[i, j] > thresh else "black")

    plt.ylabel('True label')
    plt.xlabel('Predicted label')
    plt.tight_layout()


if __name__ == "__main__":
    # load_cvss_data('./data/2017_Jan/', 0, reg=False)
    load_seq_data('data/10_days/2017-07/')
    # import pandas as pd
    # result = pd.read_csv('modeldir/basic_0/pred_true_test.csv', header=None)
    # result_np = result.values
    # y_pred = result_np[:, 0]
    # y_true = result_np[:, 1]
    # num_class = len(np.unique(y_true))
    # save_confusion_matrix(y_pred, y_true, num_class, 'metric_0')
