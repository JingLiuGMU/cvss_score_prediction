import numpy as np
import pandas as pd
import tensorflow as tf
from tensorflow.keras.preprocessing.sequence import pad_sequences
from keras.preprocessing.text import Tokenizer
import nltk, re, time
from nltk.corpus import stopwords
from nltk.stem import SnowballStemmer
from nltk.tokenize import TweetTokenizer
from bs4 import BeautifulSoup
import pickle, argparse
import warnings
warnings.filterwarnings("ignore", category=UserWarning, module='bs4')


def load_sentence_data(file_name):
    '''load data from .csv file'''
    csv_file = pd.read_pickle(file_name + 'sentences.pkl')
    x = {csv_file["cve_id"][i]: csv_file["text"][i] for i in csv_file.index}
    # x = {csv_file["cve_id"][i]: csv_file["text"][i] for i in csv_file.index}
    return x


def review_to_wordlist(review, remove_stopwords=True, stem_words=True):
    # Clean the text, with the option to remove stopwords and stem words.

    # Remove HTML
    review_text = BeautifulSoup(review, "lxml").get_text()

    # Convert words to lower case and split them
    review_text = review_text.lower()

    # Optionally remove stop words (true by default)
    if remove_stopwords:
        words = review_text.split()
        stops = set(stopwords.words("english"))
        words = [w for w in words if not w in stops]
        review_text = " ".join(words)

    # Clean the text
    review_text = re.sub(r'(https|http)?:\/\/(\w|\.|\/|\?|\=|\&|\%)*\b', '', review_text, flags=re.MULTILINE)  # remove url
    review_text = re.sub(r"[^A-Za-z0-9!?\'\`]", " ", review_text)
    review_text = re.sub(r"it's", " it is", review_text)
    review_text = re.sub(r"that's", " that is", review_text)
    review_text = re.sub(r"\'s", " 's", review_text)
    review_text = re.sub(r"\'ve", " have", review_text)
    review_text = re.sub(r"won't", " will not", review_text)
    review_text = re.sub(r"don't", " do not", review_text)
    review_text = re.sub(r"can't", " can not", review_text)
    review_text = re.sub(r"cannot", " can not", review_text)
    review_text = re.sub(r"n\'t", " n\'t", review_text)
    review_text = re.sub(r"\'re", " are", review_text)
    review_text = re.sub(r"\'d", " would", review_text)
    review_text = re.sub(r"\'ll", " will", review_text)
    review_text = re.sub(r"!", " ! ", review_text)
    review_text = re.sub(r"\?", " ? ", review_text)
    review_text = re.sub(r"\s{2,}", " ", review_text)
    # remove numbers
    #review_text = re.sub(r'cve', '', review_text)
    #review_text = re.sub(r'cves', '', review_text)
    #review_text = re.sub(r'\d +', '', review_text)

    # Shorten words to their stems
    if stem_words:
        words = review_text.split()
        stemmer = SnowballStemmer('english')
        stemmed_words = [stemmer.stem(word) for word in words]
        # customized_stops = set(['cve', 'https', 'cves'])
        # stemmed_words = [w for w in stemmed_words if not w in customized_stops]
        review_text = " ".join(stemmed_words)
    # review_text = re.sub(r'\d +', '', review_text)
    # Return a list of words, with each word as its own string
    return review_text


def review_to_sentences(raw_sentences):
    # Split a review into parsed sentences
    # Returns a list of sentences, where each sentence is a list of words

    # Use the NLTK tokenizer to split the review into sentences
    # raw_sentences = tokenizer.tokenize(review.strip())
    tknzr = TweetTokenizer(strip_handles=True, reduce_len=True)
    sentences = []
    for raw_sentence in raw_sentences:
        raw_sentence = tknzr.tokenize(raw_sentence)
        raw_sentence = " ".join(raw_sentence)
        # If a sentence contains less than 5 words, skip it
        if len(raw_sentence.strip()) > 20:
            # Otherwise, call review_to_wordlist to get a list of words
            wordlist = review_to_wordlist(raw_sentence)
            if len(wordlist) > 0:
                sentences.append(wordlist)

    # Return the list of sentences
    # Each sentence is a list of words, so this returns a list of lists
    return sentences


def save_original_word_dict(filepath, word_dict):
    import csv
    with open(filepath + 'dict.csv', 'w', newline="") as csv_file:
        writer = csv.writer(csv_file)
        for key, value in word_dict.items():
            writer.writerow([key, value])


def text_preprocess(filepath, k=10):
    data = load_sentence_data(filepath)
    tknzr = Tokenizer()
    print("Parsing sentences from original dataset...")
    sentences = {}
    for key, item in data.items():
        # if int(key) > 100:
        #     break
        sentences[key] = review_to_sentences(item)
    # max_seq_length = get_seq_length(sentences)
    max_seq_length = 50
    print('max seq length{}'.format(max_seq_length))
    all_text = []
    for key, item in sentences.items():
        all_text += item
    print("Fitting Tokenizer to text...")
    # all_text = ' '.join(all_text)
    tknzr.fit_on_texts(all_text)
    print("Fitting is complete.")
    word_index = tknzr.word_index
    print("Original Words in index: %d" % len(word_index))

    # save_original_word_dict(filepath, tknzr.word_counts)
    # with open(filepath + 'word_dict_original.pkl', 'wb') as fout:
    #     pickle.dump(tknzr.word_counts, fout, protocol=pickle.HIGHEST_PROTOCOL)
    # # max_freq, min_freq = get_freq_range(tknzr.word_counts, k)
    # # print('max freq {}, min freq {}'.format(max_freq, min_freq))
    tknzr = modify_tokenizer(tknzr, 30, 5)
    word_index = tknzr.word_index
    print("Words in index: %d" % len(word_index))
    with open(filepath + 'word_dict.pkl', 'wb') as fout:
        pickle.dump(tknzr.word_counts, fout, protocol=pickle.HIGHEST_PROTOCOL)
    with open(filepath + 'word_docs.pkl', 'wb') as fout:
        pickle.dump(tknzr.word_docs, fout, protocol=pickle.HIGHEST_PROTOCOL)
    print("Words in docs: %d" % len(tknzr.word_docs))
    tweet_seq = {}

    for key, items in sentences.items():
        temp_seq = tknzr.texts_to_sequences(items)
        # temp_seq = tknzr.texts_to_matrix(items)
        temp_pad_seq = pad_sequences(temp_seq, maxlen=max_seq_length, padding='post', truncating='post')
        tweet_seq[key] = np.unique(temp_pad_seq, axis=0)  # keep unique seqs for each cve
    # get_seq_length(tweet_seq)

    with open(filepath + 'tweet_seq.pkl', 'wb') as handle:
        pickle.dump(tweet_seq, handle, protocol=pickle.HIGHEST_PROTOCOL)
    return tweet_seq, len(tknzr.word_counts) + 1


def get_freq_range(dicts, k):
    import operator
    sorted_dict = sorted(dicts.items(), key=operator.itemgetter(1))

    top_k_freq = sorted_dict[-k][1]
    low_k_freq = sorted_dict[k][1]
    return top_k_freq, low_k_freq


def modify_tokenizer(tokenizer, top_freq=80, bottom_freq=5):
    '''delete words whose frequences are less than bottom_freq and less than top_freq'''
    low_count_words = [w for w, c in tokenizer.word_counts.items() if (c <= bottom_freq) | (c >= top_freq) | (len(w) < 2)]
    print('deleted words {}'.format(low_count_words))
    for w in low_count_words:
        del tokenizer.word_index[w]
        del tokenizer.word_docs[w]
        del tokenizer.word_counts[w]
    tokenizer.word_index = dict([(y, x) for x, y in enumerate(tokenizer.word_index.keys())])
    tokenizer.word_docs = dict([(y, x) for x, y in enumerate(tokenizer.word_docs.keys())])
    tokenizer.word_counts = dict([(y, x) for x, y in enumerate(tokenizer.word_counts.keys())])
    return tokenizer


def get_seq_length(tweet_seq):
    length = []
    for key, seqs in tweet_seq.items():
        for seq in seqs:
            length.append(len(seq))
    length_df = pd.DataFrame(length, columns=['counts'])
    print(length_df.counts.describe())
    print("80% percentile: {}".format(np.percentile(length_df.counts, 80)))
    print("85% percentile: {}".format(np.percentile(length_df.counts, 85)))
    print("90% percentile: {}".format(np.percentile(length_df.counts, 90)))
    print("95% percentile: {}".format(np.percentile(length_df.counts, 95)))
    return int(np.percentile(length_df.counts, 50))


def get_seq_num(tweet_seq):
    num = []
    for key, seqs in tweet_seq.items():
        num.append(seqs.shape[0])

    length_df = pd.DataFrame(num, columns=['num'])
    print(length_df.num.describe())
    print(length_df.num.max(), length_df.num.min())
    print("80% percentile: {}".format(np.percentile(length_df.num, 80)))
    print("85% percentile: {}".format(np.percentile(length_df.num, 85)))
    print("90% percentile: {}".format(np.percentile(length_df.num, 90)))
    print("95% percentile: {}".format(np.percentile(length_df.num, 95)))


if __name__ == "__main__":
    parse = argparse.ArgumentParser()

    parse.add_argument('--filepath', '-f', type=str, default='data/10_days/2017-08/')

    args = parse.parse_args()
    data = text_preprocess(args.filepath)
    with open(args.filepath + 'tweet_seq.pkl', 'rb') as fin:
        seq = pickle.load(fin)
    get_seq_num(seq)
