import os
import time
import random
import numpy as np
import networkx as nx
import tensorflow as tf
from tensorflow.contrib.rnn import BasicLSTMCell
import ops
from utils import load_data, preprocess_features, preprocess_adj, load_cvss_data, calc_f1, cal_evl_metric, load_seq_data, cal_auc
import utils
from batch_utils import get_sampled_index, get_indice_graph
import csv


class GraphNet(object):

    def __init__(self, sess, conf):
        self.sess = sess
        # customize modeldir and logdir based on experiment name
        if conf.exp_name:
            exp_name = (conf.exp_name.format(**(conf.flag_values_dict()))) if conf.exp_name != '' else ''
            exp_name = exp_name.replace(', ', '+')
            exp_name = exp_name.replace('[', '')
            exp_name = exp_name.replace(']', '')
            conf.modeldir = conf.modeldir + exp_name + '/'
            conf.logdir = conf.logdir + exp_name + '/'

        if not os.path.exists(conf.modeldir):
            os.makedirs(conf.modeldir)
        if not os.path.exists(conf.logdir):
            os.makedirs(conf.logdir)
        self.conf = conf
        self.process_data()
        self.configure_networks()
        self.train_summary = self.config_summary('train')
        self.valid_summary = self.config_summary('valid')
        self.test_summary = self.config_summary('test')

    def bi_lstm(self, batch_embedded):
        def lstm_cell():
            lstm = tf.contrib.rnn.BasicLSTMCell(self.conf.hidden_size, forget_bias=1.0,
                                                state_is_tuple=True, reuse=tf.get_variable_scope().reuse)
            return lstm

        cell_fw = tf.contrib.rnn.MultiRNNCell([lstm_cell() for _ in range(self.conf.layers)])
        cell_bw = tf.contrib.rnn.MultiRNNCell([lstm_cell() for _ in range(self.conf.layers)])

        self._initial_state_fw = cell_fw.zero_state(self.batch_size_lstm, dtype=tf.float32)
        self._initial_state_bw = cell_bw.zero_state(self.batch_size_lstm, dtype=tf.float32)

        # Dynamic Bi-LSTM
        with tf.variable_scope('Bi-LSTM'):
            _, state = tf.nn.bidirectional_dynamic_rnn(cell_fw,
                                                       cell_bw,
                                                       inputs=batch_embedded,
                                                       initial_state_fw=self._initial_state_fw,
                                                       initial_state_bw=self._initial_state_bw)
                                                       #sequence_length=self.sequence_length)

        state_fw = state[0]
        state_bw = state[1]
        output = tf.concat([state_fw[self.conf.layers - 1].h, state_bw[self.conf.layers - 1].h], 1)
        print(output)

        return output

    def embgpu(self):
        with tf.device(f"/gpu:1"):
            var = tf.get_variable('embedding', [self.vocab_size, self.conf.embedding_size]) #tf.Variable(tf.random_uniform([self.vocab_size, self.conf.embedding_size], -1.0, 1.0),
                                         #trainable=True, name='embedding')
        return var

    def LSTM_embedding(self):
        if self.conf.raw_seq:
            batch_embedded = tf.reshape(self.seq_tf, [-1, self.seq_length, 1])
        else:
            embeddings_var = tf.Variable(tf.random_uniform([self.vocab_size, self.conf.embedding_size], -1.0, 1.0),
                                         trainable=True, name='embedding')
            batch_embedded = tf.nn.embedding_lookup(embeddings_var, self.seq_tf)

        output_states = self.bi_lstm(batch_embedded)

        attention_output = ops.fully_connected(output_states, self.conf.attention_size, 'attention',
                                               self.slope_tensor, act_fn=tf.nn.sigmoid)
        # print('attention output shape', attention_output)
        embeds = attention_output * output_states

        embeds = tf.reshape(embeds, [-1, self.conf.num_sentences, 2 * self.conf.hidden_size])
        # print('embed after reshape: ', embeds)
        if 'max' in self.conf.pooling:
            embeds = tf.reduce_max(embeds, axis=1)
        elif 'mean' in self.conf.pooling:
            embeds = tf.reduce_mean(embeds, axis=1)

        return embeds

    def inference(self, outs):
        if self.conf.LSTM_embed:
            lstm_embed = self.LSTM_embedding()
            self.avg_lstm_tf = self.conf.w * lstm_embed + (1 - self.conf.w) * self.avg_lstm_prev
            outs = tf.concat([self.avg_lstm_tf, outs], axis=-1, name='concat_lstm')
            print('after concate, outs shape', outs)

        outs = getattr(ops, self.conf.first_conv)(
            self.normed_matrix, outs, 4 * self.conf.ch_num, self.conf.adj_keep_r,
            self.conf.keep_r, self.is_train, 'conv_s', act_fn=None)
        for layer_index in range(self.conf.layer_num):
            cur_outs = getattr(ops, self.conf.second_conv)(
                self.normed_matrix, outs, self.conf.ch_num, self.conf.adj_keep_r,
                self.conf.keep_r, self.is_train, 'conv_%s' % (layer_index + 1),
                act_fn=None, k=self.conf.k)
            outs = tf.concat([outs, cur_outs], axis=1, name='concat_%s' % layer_index)
        if self.conf.what == 'classification':
            outs = ops.simple_conv(
                self.normed_matrix, outs, self.conf.class_num, self.conf.adj_keep_r,
                self.conf.keep_r, self.is_train, 'conv_f', act_fn=None, norm=False)
        elif self.conf.what == 'reg':
            for layer_index in range(self.conf.fc_layer_num):
                num_out = outs.shape[-1].value
                num_out = num_out // 2 + 1
                outs = ops.simple_conv(self.normed_matrix,
                                       outs, num_out, self.conf.adj_keep_r,
                                       self.conf.keep_r,
                                       self.is_train, 'linear_output_l%s' % layer_index, act_fn=tf.nn.relu, norm=False)
            outs = ops.simple_conv(
                self.normed_matrix, outs, 1, self.conf.adj_keep_r,
                self.conf.keep_r, self.is_train, 'conv_f', act_fn=tf.nn.sigmoid, norm=False)
        return outs

    def get_optimizer(self, lr):
        return tf.contrib.opt.NadamOptimizer(lr)

    def process_data(self):
        if self.conf.what == 'classification':
            data = load_cvss_data(self.conf.dataset_prefix, self.conf.metrics_id,
                                  feat_type=self.conf.feat_type, threshold=self.conf.threshold)
        elif self.conf.what == 'reg':
            data = load_cvss_data(self.conf.dataset_prefix, reg=True,
                                  feat_type=self.conf.feat_type, threshold=self.conf.threshold)
        else:
            raise NotImplementedError('mode {} is not supported!'.format(self.conf.what))
        adj, feas = data[:2]
        self.adj = adj
        self.normed_adj = preprocess_adj(adj)
        self.feas = preprocess_features(feas, False)
        self.y = data[2]
        self.train_mask, self.val_mask, self.test_mask, class_weights = data[3:]
        if self.conf.what == 'classification':
            self.class_weights = class_weights.reshape((1, self.conf.class_num))
        else:
            self.class_weights = 0
        self.seq, self.vocab_size = load_seq_data(self.conf.dataset_prefix)
        self.seq_length = self.seq[0].shape[1]
        self.avg_lstm_init = np.zeros((adj.shape[0], 2 * self.conf.hidden_size))

    def configure_networks(self):
        self.build_network()
        self.cal_loss()
        optimizer = self.get_optimizer(self.conf.learning_rate)
        # tfprint = tf.Print([tf.shape(self.loss_op)], [tf.shape(self.loss_op)], 'loss op shape: ')
        # with tf.control_dependencies([tfprint]):
        self.train_op = optimizer.minimize(self.loss_op, name='train_op')
        self.seed = int(time.time())
        tf.set_random_seed(self.seed)
        self.sess.run(tf.global_variables_initializer())
        trainable_vars = tf.trainable_variables()
        self.saver = tf.train.Saver(var_list=trainable_vars, max_to_keep=0)
        if self.conf.is_train:
            self.writer = tf.summary.FileWriter(self.conf.logdir, self.sess.graph)
        self.print_params_num()

    def build_network(self):
        self.labels_mask = tf.placeholder(tf.int32, None, name='labels_mask')
        self.matrix = tf.placeholder(tf.int32, [None, None], name='matrix')
        self.normed_matrix = tf.placeholder(tf.float32, [None, None], name='normed_matrix')
        self.inputs = tf.placeholder(tf.float32, [None, self.feas.shape[1]], name='inputs')
        self.batch_size_lstm = tf.placeholder(dtype=tf.int32, shape=[], name='batch_size_lstm')
        self.sequence_length = tf.placeholder(dtype=tf.int32, shape=[None], name='sequence_length')
        if self.conf.what == 'classification':
            self.labels = tf.placeholder(tf.int32, [None, self.conf.class_num], name='labels')
            self.weights = tf.placeholder(tf.float32, [1, self.conf.class_num], name='class_weights')
        else:
            self.labels = tf.placeholder(tf.float32, None, name='labels')
            self.weights = tf.placeholder(tf.int32)
        self.is_train = tf.placeholder(tf.bool, name='is_train')
        # self.seq_tf = tf.placeholder(tf.int32, [None, None, self.seq[0].shape[1]])  # Nv x n x d
        self.seq_tf = tf.placeholder(tf.int32, [None, self.seq_length])  # (Nv*n) x seq_len

        self.slope_tensor = tf.placeholder(tf.float32, None, name='slope_tensor')
        self.avg_lstm_prev = tf.placeholder(tf.float32, shape=[None, 2 * self.conf.hidden_size], name="curr_avg")
        # self.avg_lstm_weight = tf.placeholder(tf.float32, name='moving_average_weight')

        self.preds = self.inference(self.inputs)
        self.sfx_preds = tf.nn.softmax(self.preds)

    def cal_loss(self):
        with tf.variable_scope('loss'):
            if self.conf.what == 'classification':
                if self.conf.weighted_loss:
                    # class_weights = tf.constant()
                    self.class_loss = ops.masked_softmax_cross_entropy(
                        self.preds, self.labels, self.labels_mask, self.weights)
                else:
                    self.class_loss = ops.masked_softmax_cross_entropy(
                        self.preds, self.labels, self.labels_mask)

                self.accuracy_op = ops.masked_accuracy(self.preds, self.labels, self.labels_mask)
            elif self.conf.what == 'reg':
                self.class_loss = ops.masked_mse(self.preds, self.labels, self.labels_mask)
                self.accuracy_op = tf.constant(0)
            self.regu_loss = 0
            for var in tf.trainable_variables():
                self.regu_loss += self.conf.weight_decay * tf.nn.l2_loss(var)
            self.loss_op = self.class_loss + self.regu_loss
            print('loss op', self.loss_op)

    def config_summary(self, name):
        summarys = []
        summarys.append(tf.summary.scalar(name + '/loss', self.loss_op))
        summarys.append(tf.summary.scalar(name + '/class_loss', self.class_loss))
        if name == 'train':
            summarys.append(tf.summary.scalar(name + '/regu_loss', self.regu_loss))
        summary = tf.summary.merge(summarys)
        return summary

    def save_summary(self, summary, step):
        self.writer.add_summary(summary, step)

    def train(self):
        if self.conf.reload_step > 0:
            self.reload(self.conf.reload_step)
        self.new_trans_train()

    def new_trans_train(self):
        avg_lstm_prev_whole = self.avg_lstm_init
        feed_train_dict, indices = self.pack_trans_dict('train', avg_lstm_prev_whole)
        stats = [-1000, 0, 0]
        preds = np.zeros_like(self.y)

        for epoch_num in range(self.conf.max_step + 1):
            if self.conf.LSTM_embed & self.conf.slope_anneal:
                # update sigmoid slope for slope annealing
                slope_tensor = min(self.conf.max_slope, 1 + 0.004 * epoch_num)  # https://arxiv.org/pdf/1609.01704.pdf
                feed_train_dict.update({self.slope_tensor: slope_tensor})
            train_loss, _, summary, train_accuracy, all_pred, avg_lstm_prev = self.sess.run(
                [self.loss_op, self.train_op, self.train_summary, self.accuracy_op, self.sfx_preds,
                 self.avg_lstm_tf],
                feed_dict=feed_train_dict)
            self.save_summary(summary, epoch_num + self.conf.reload_step)
            if self.conf.model_select == 'accuracy':
                evl_value = train_accuracy
            elif self.conf.model_select == 'auc':
                train_auc = cal_auc(self.y, all_pred, self.train_mask)
                evl_value = train_auc
            else:
                evl_value = -1 * train_loss
            if evl_value >= stats[0]:
                stats[0] = evl_value
                if self.conf.use_batch:
                    preds[indices] = all_pred
                    avg_lstm_prev_whole[indices] = avg_lstm_prev
                    feed_train_dict, indices = self.pack_trans_dict('train', avg_lstm_prev_whole)
                else:
                    preds = all_pred
            if epoch_num and epoch_num % 100 == 0:
                self.save(epoch_num)
            print('step: %d --- loss: %.4f, train accuracy: %.3f' % (
                epoch_num, train_loss, train_accuracy))
        self.new_evaluate(preds, self.y, self.test_mask, 'test', True)

    def new_evaluate(self, preds, labels, mask, type, save=False):
        mac_recall, mac_prec, mic_recall, mic_prec, mic, mac, accuracy, y_pred, y_true = \
            calc_f1(labels, preds, mask)
        if save:
            utils.save_confusion_matrix(y_true, y_pred, self.conf.class_num, type, self.conf.modeldir)
            if type == "test":
                mask = mask.astype(bool)
                preds_temp = preds[mask]
                if self.conf.layer_num == 0:
                    np.savetxt(self.conf.dataset_prefix + "ma_pred_true_metric_" + str(self.conf.metrics_id) + "_ths_" +
                               str(self.conf.threshold) + "_lstm_only.csv",
                               np.c_[y_pred, y_true, preds_temp], delimiter=",")
                elif self.conf.weighted_loss & (self.conf.model_select == 'accuracy'):
                    np.savetxt(self.conf.dataset_prefix + "ma_pred_true_metric_" + str(self.conf.metrics_id) + "_ths_" +
                               str(self.conf.threshold) + "_weighted.csv",
                               np.c_[y_pred, y_true, preds_temp], delimiter=",")
                elif self.conf.model_select == 'accuracy':
                    np.savetxt(self.conf.dataset_prefix + "ma_pred_true_metric_" + str(self.conf.metrics_id) + "_ths_" +
                               str(self.conf.threshold) + '_feat_' + self.conf.feat_type + ".csv",
                               np.c_[y_pred, y_true, preds_temp], delimiter=",")
                else:
                    np.savetxt(self.conf.dataset_prefix + "ma_pred_true_metric_" + str(self.conf.metrics_id) + "_ths_" +
                               str(self.conf.threshold) + "_" + self.conf.model_select + '_weighted_' +
                               str(self.conf.weighted_loss) + ".csv",
                               np.c_[y_pred, y_true, preds_temp], delimiter=",")

        if not os.path.isfile(self.conf.metrics_filename):
            writer = csv.writer(open(self.conf.metrics_filename, 'w+', newline=''))
            res_summary = []
            res_summary.extend(
                ['type', 'metric_id', 'mac_recall', 'mac_precision', 'mac_f1', 'mic_recall', 'mic_precision', 'mic_f1',
                 'accuracy', 'threshold', 'lr', 'ch_num', 'k', 'feat_type', 'dateset', 'keep_r', 'adj_keep_r', 'l2',
                 'Embedding', 'Embedding_size', 'hidden_size', 'pooling', 'slope_anneal', 'max_slope', 'weighted_loss',
                 'subgraph', 'batch_size', 'center_num'])
            writer.writerow(res_summary)
        else:
            writer = csv.writer(open(self.conf.metrics_filename, 'a', newline=''))
        res_summary = [type, self.conf.metrics_id, mac_recall, mac_prec, mac, mic_recall, mic_prec, mic, accuracy,
                       self.conf.threshold,
                       self.conf.learning_rate, self.conf.ch_num, self.conf.k, self.conf.feat_type,
                       self.conf.dataset_prefix, self.conf.keep_r, self.conf.adj_keep_r, self.conf.weight_decay,
                       self.conf.LSTM_embed, self.conf.embedding_size, self.conf.hidden_size, self.conf.pooling,
                       self.conf.slope_anneal,
                       self.conf.max_slope, self.conf.weighted_loss, self.conf.use_batch, self.conf.batch_size,
                       self.conf.center_num]
        print('{} metrics: mac_recall {}, mac_prec {}, Mac_f1 {}, mic_recall {}, mic_precision {}, mic_f1 {}'
              ' np_accuracy {}'
              .format(type, mac_recall, mac_prec, mac, mic_recall, mic_prec, mic, accuracy))
        writer.writerow(res_summary)

    def transductive_train(self):
        avg_lstm_prev = self.avg_lstm_init
        print('avg lstm init shape {} '.format(avg_lstm_prev.shape))
        feed_train_dict = self.pack_trans_dict('train', avg_lstm_prev)
        feed_valid_dict = self.pack_trans_dict('valid', avg_lstm_prev)
        feed_test_dict = self.pack_trans_dict('test', avg_lstm_prev)
        stats = [0, 0, 0]
        for epoch_num in range(self.conf.max_step + 1):

            if self.conf.LSTM_embed & self.conf.slope_anneal:
                # update sigmoid slope for slope annealing
                slope_tensor = min(self.conf.max_slope, 1 + 0.004 * epoch_num)  # https://arxiv.org/pdf/1609.01704.pdf
                feed_train_dict.update({self.slope_tensor: slope_tensor})
                feed_valid_dict.update({self.slope_tensor: slope_tensor})
                feed_test_dict.update({self.slope_tensor: slope_tensor})

            train_loss, _, summary, train_accuracy, avg_lstm_prev = self.sess.run(
                [self.loss_op, self.train_op, self.train_summary, self.accuracy_op, self.avg_lstm_tf],
                feed_dict=feed_train_dict)
            print('avg lstm prev shape {} for epoch {}'.format(avg_lstm_prev.shape, epoch_num))
            self.save_summary(summary, epoch_num + self.conf.reload_step)
            summary, valid_accuracy = self.sess.run(
                [self.valid_summary, self.accuracy_op],
                feed_dict=feed_valid_dict)
            self.save_summary(summary, epoch_num + self.conf.reload_step)
            summary, test_accuracy = self.sess.run(
                [self.test_summary, self.accuracy_op],
                feed_dict=feed_test_dict)
            self.save_summary(summary, epoch_num + self.conf.reload_step)
            if self.conf.LSTM_embed:
                feed_train_dict.update({self.avg_lstm_prev: avg_lstm_prev})
                feed_valid_dict.update({self.avg_lstm_prev: avg_lstm_prev})
                feed_test_dict.update({self.avg_lstm_prev: avg_lstm_prev})

            if train_accuracy >= stats[0]:
                stats[0], stats[1], stats[2] = train_accuracy, 0, max(test_accuracy, stats[2])
                self.saver.save(self.sess, os.path.join(self.conf.modeldir, "best.ckpt"))
            else:
                stats[1] += 1
            if epoch_num and epoch_num % 100 == 0:
                self.save(epoch_num)
            print('step: %d --- loss: %.4f, train accuracy: %.3f, val accuracy: %.3f' % (
                epoch_num, train_loss, train_accuracy, valid_accuracy))
            if stats[1] > 150 and epoch_num > 150:
                print('Test accuracy -----> ', self.seed, stats[2])
                break
        self.saver.restore(self.sess, os.path.join(self.conf.modeldir, "best.ckpt"))
        if self.conf.what == 'classification':
            self.evaluate(feed_train_dict, 'train', True)
            self.evaluate(feed_valid_dict, 'val', True)
            self.evaluate(feed_test_dict, 'test', True)
        else:
            self.eval_reg(feed_test_dict, 'test', True)

    def eval_reg(self, feed_dict, type, save=False):
        outs = self.sess.run([self.preds, self.labels, self.labels_mask],
                             feed_dict=feed_dict)
        r2, mae, mse, mape, y_true, y_pred = cal_evl_metric(outs[0], outs[1], outs[2])
        print('{} metrics: r2 {}, mae {}, mase {}, mape {}'.format(type, r2, mae, mse, mape))

        # filename = 'data/reg_metrics.csv'
        if not os.path.isfile(self.conf.metrics_filename):
            writer = csv.writer(open(self.conf.metrics_filename, 'w+', newline=''))
            res_summary = []
            res_summary.extend(
                ['type', 'r2', 'mae', 'mase', 'mape', 'threshold', 'lr', 'ch_num', 'k', 'feat_type', 'dateset', 'keep_r',
                 'adj_keep_r', 'l2', 'Embedding',
                 'Embedding_size', 'hidden_size', 'pooling', 'slope_anneal', 'max_slope', 'weighted_loss',
                 'subgraph', 'batch_size', 'center_num'])
            writer.writerow(res_summary)
        else:
            writer = csv.writer(open(self.conf.metrics_filename, 'a', newline=''))
        res_summary = [type, r2, mae, mse, mape, self.conf.threshold,
                       self.conf.learning_rate, self.conf.ch_num, self.conf.k, self.conf.feat_type,
                       self.conf.dataset_prefix, self.conf.keep_r, self.conf.adj_keep_r, self.conf.weight_decay,
                       self.conf.LSTM_embed, self.conf.embedding_size, self.conf.hidden_size, self.conf.pooling,
                       self.conf.slope_anneal,
                       self.conf.max_slope, self.conf.weighted_loss, self.conf.use_batch, self.conf.batch_size,
                       self.conf.center_num]
        writer.writerow(res_summary)
        if save:
            print(self.conf.modeldir)
            np.savetxt(self.conf.modeldir + "pred_true_test_reg.csv", np.c_[outs[0], outs[1]], delimiter=",")

    def evaluate(self, feed_dict, type, save=False):
        outs = self.sess.run([self.preds, self.labels, self.labels_mask, self.accuracy_op],
                             feed_dict=feed_dict)
        mac_recall, mac_prec, mic_recall, mic_prec, mic, mac, accuracy, y_pred, y_true = \
            calc_f1(outs[1], outs[0], outs[2])
        if save:
            utils.save_confusion_matrix(y_true, y_pred, self.conf.class_num, type, self.conf.modeldir)
            if type == "test":
                np.savetxt(self.conf.dataset_prefix + "ma_pred_true_metric_" + str(self.conf.metrics_id) + "_ths_" +
                           str(self.conf.threshold) + ".csv",
                           np.c_[y_pred, y_true], delimiter=",")

        if not os.path.isfile(self.conf.metrics_filename):
            writer = csv.writer(open(self.conf.metrics_filename, 'w+', newline=''))
            res_summary = []
            res_summary.extend(
                ['type', 'metric_id', 'mac_recall', 'mac_precision', 'mac_f1', 'mic_recall', 'mic_precision', 'mic_f1',
                 'accuracy', 'threshold', 'lr', 'ch_num', 'k', 'feat_type', 'dateset', 'keep_r', 'adj_keep_r', 'l2',
                 'Embedding', 'Embedding_size', 'hidden_size', 'pooling', 'slope_anneal', 'max_slope', 'weighted_loss',
                 'subgraph', 'batch_size', 'center_num'])
            writer.writerow(res_summary)
        else:
            writer = csv.writer(open(self.conf.metrics_filename, 'a', newline=''))
        res_summary = [type, self.conf.metrics_id, mac_recall, mac_prec, mac, mic_recall, mic_prec, mic, accuracy,
                       self.conf.threshold,
                       self.conf.learning_rate, self.conf.ch_num, self.conf.k, self.conf.feat_type,
                       self.conf.dataset_prefix, self.conf.keep_r, self.conf.adj_keep_r, self.conf.weight_decay,
                       self.conf.LSTM_embed, self.conf.embedding_size, self.conf.hidden_size, self.conf.pooling,
                       self.conf.slope_anneal,
                       self.conf.max_slope, self.conf.weighted_loss, self.conf.use_batch, self.conf.batch_size,
                       self.conf.center_num]
        print('{} metrics: mac_recall {}, mac_prec {}, Mac_f1 {}, mic_recall {}, mic_precision {}, mic_f1 {}'
              'np_accuracy {}, tf_accuracy {}'
              .format(type, mac_recall, mac_prec, mac, mic_recall, mic_prec, mic, accuracy, outs[3]))
        writer.writerow(res_summary)
        # return outs[3], recall, prec, mic, mac, accuracy

    def pack_trans_dict(self, action, avg_lstm_prev):
        seqs = ops.select_seqs(self.seq, self.conf.num_sentences)  # (Nv*n) x seq_length
        indices = None
        # print('@@@@@@original seqs dimension: {}'.format(seqs.shape))
        feed_dict = {
            self.matrix: self.adj, self.normed_matrix: self.normed_adj, self.batch_size_lstm: seqs.shape[0],
            self.inputs: self.feas, self.labels: self.y, self.seq_tf: seqs, self.weights: self.class_weights,
            self.avg_lstm_prev: avg_lstm_prev
        }
        if action == 'train':
            feed_dict.update({
                self.labels_mask: self.train_mask,
                self.is_train: True})
            if self.conf.use_batch:
                # indices = get_indice_graph(
                #     self.adj, self.train_mask, self.conf.batch_size, 1.0)

                indices = get_sampled_index(self.adj, self.conf.batch_size, self.conf.center_num)
                #print('sampled indices {}'.format(len(indices)))
                new_adj = self.adj[indices, :][:, indices]
                new_normed_adj = self.normed_adj[indices, :][:, indices]
                seqs = np.reshape(seqs, (-1, self.conf.num_sentences, self.seq_length))
                # print('seqs', seqs)

                new_seqs = np.reshape(seqs[indices], [-1, self.seq_length])
                feed_dict.update({
                    self.labels: self.y[indices],
                    self.labels_mask: self.train_mask[indices],
                    self.matrix: new_adj, self.normed_matrix: new_normed_adj,
                    self.inputs: self.feas[indices],
                    self.seq_tf: new_seqs, self.batch_size_lstm: new_seqs.shape[0],
                    self.avg_lstm_prev: avg_lstm_prev[indices]})
        elif action == 'valid':
            feed_dict.update({
                self.labels: self.y, self.labels_mask: self.val_mask,
                self.is_train: False})
            if self.conf.use_batch:
                #indices = get_sampled_index(self.adj, self.conf.batch_size, self.conf.center_num)
                indices = get_indice_graph(
                    self.adj, self.val_mask, 10000, 1.0)
                new_adj = self.adj[indices, :][:, indices]
                new_normed_adj = self.normed_adj[indices, :][:, indices]
                seqs = np.reshape(seqs, (-1, self.conf.num_sentences, self.seq_length))
                # print('seqs', seqs)

                new_seqs = np.reshape(seqs[indices], [-1, self.seq_length])
                feed_dict.update({
                    self.labels: self.y[indices],
                    self.labels_mask: self.val_mask[indices],
                    self.matrix: new_adj, self.normed_matrix: new_normed_adj,
                    self.inputs: self.feas[indices],
                    self.seq_tf: new_seqs, self.batch_size_lstm: new_seqs.shape[0],
                    self.avg_lstm_prev: avg_lstm_prev[indices]})
        else:
            feed_dict.update({
                self.labels: self.y, self.labels_mask: self.test_mask,
                self.is_train: False})
            if self.conf.use_batch:
                indices = get_indice_graph(
                    self.adj, self.test_mask, 10000, 1.0)
                new_adj = self.adj[indices, :][:, indices]
                new_normed_adj = self.normed_adj[indices, :][:, indices]
                seqs = np.reshape(seqs, (-1, self.conf.num_sentences, self.seq_length))
                # print('seqs', seqs)

                new_seqs = np.reshape(seqs[indices], [-1, self.seq_length])
                feed_dict.update({
                    self.labels: self.y[indices],
                    self.labels_mask: self.test_mask[indices],
                    self.matrix: new_adj, self.normed_matrix: new_normed_adj,
                    self.inputs: self.feas[indices],
                    self.seq_tf: new_seqs, self.batch_size_lstm: new_seqs.shape[0],
                    self.avg_lstm_prev: avg_lstm_prev[indices]})
        return feed_dict, indices

    def save(self, step):
        print('---->saving', step)
        checkpoint_path = os.path.join(
            self.conf.modeldir, self.conf.model_name)
        self.saver.save(self.sess, checkpoint_path, global_step=step)

    def reload(self, step):
        checkpoint_path = os.path.join(
            self.conf.modeldir, self.conf.model_name)
        model_path = checkpoint_path + '-' + str(step)
        if not os.path.exists(model_path + '.meta'):
            print('------- no such checkpoint', model_path)
            return
        self.saver.restore(self.sess, model_path)

    def print_params_num(self):
        total_params = 0
        for var in tf.trainable_variables():
            print(var)
            total_params += var.shape.num_elements()
        print("The total number of params --------->", total_params)
