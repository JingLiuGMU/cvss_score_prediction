import os
import argparse
import tensorflow as tf
from network import GraphNet


####Delete all flags before declare#####
def del_all_flags(FLAGS):
    flags_dict = FLAGS._flags()
    keys_list = [keys for keys in flags_dict]
    for keys in keys_list:
        FLAGS.__delattr__(keys)


del_all_flags(tf.flags.FLAGS)

# training
flags = tf.app.flags
flags.DEFINE_integer('max_step', 200, '# of step for training')
flags.DEFINE_integer('summary_interval', 100, '# of step to save summary')
flags.DEFINE_float('learning_rate', 0.005, 'learning rate')
flags.DEFINE_boolean('is_train', True, 'is train')
flags.DEFINE_integer('class_num', 4, 'output class number')
flags.DEFINE_string('what', 'classification', 'classification or reg')
flags.DEFINE_string('feat_type', 'basic', 'feature types to use for prediction')
flags.DEFINE_integer('fc_layer_num', 2, 'num of fc layers for regression')
flags.DEFINE_string('metrics_filename', './data/metrics.csv',
                    'path and filename to save final evaluation metrics results')
flags.DEFINE_string('gpuid', '0', 'specify GPU id')
# Debug
flags.DEFINE_float('threshold', 0.3, 'threshold on cosine similarity when constructing graph')
flags.DEFINE_string('logdir', './logdir/', 'Log dir')
flags.DEFINE_string('modeldir', './modeldir/', 'Model dir')
flags.DEFINE_string('model_name', 'model', 'Model file name')
flags.DEFINE_integer('reload_step', 0, 'Reload step to continue training')
flags.DEFINE_integer('test_step', 0, 'Test or predict model at this step')
flags.DEFINE_string('exp_name',
                    '{what}_{dataset_prefix}_{feat_type}_{metrics_id}_embedding-{LSTM_embed}-embd-{embedding_size}'
                    '-hsize{hidden_size}-n_sent{num_sentences}-t-{threshold}'
                    '-pool-{pooling}-anneal-{slope_anneal}-max_slope-{max_slope}_weighted_loss-{weighted_loss}'
                    '_l2-{weight_decay}_lr-{learning_rate}_feat_type-{feat_type}_ch_num-{ch_num}_gcn_num-{layer_num}'
                    'keep_ratio-{keep_r}_k-{k}_subgraph-{use_batch}-size-{batch_size}-start-{center_num}',
                    'experiment name: e.g., {feat_type}_{metrics_id}')
flags.DEFINE_string('flag', 'train', 'whether to train or test')
# network architecture
flags.DEFINE_integer('ch_num', 2, 'channel number')
flags.DEFINE_integer('layer_num', 2, 'block number')
flags.DEFINE_float('adj_keep_r', 0.999, 'dropout keep rate')
flags.DEFINE_float('keep_r', 0.5, 'dropout keep rate')
flags.DEFINE_float('weight_decay', 5e-4, 'Weight for L2 loss on embedding matrix.')
flags.DEFINE_integer('k', 80, 'top k')
flags.DEFINE_string('first_conv', 'simple_conv', 'simple_conv, chan_conv')
flags.DEFINE_string('second_conv', 'graph_conv', 'graph_conv, simple_conv')
flags.DEFINE_boolean('use_batch', True, 'use batch training')
flags.DEFINE_integer('batch_size', 500, 'batch size number')
flags.DEFINE_integer('center_num', 150, 'start center number')
flags.DEFINE_string('dataset_prefix', 'data/10_days/2017-10/', 'path to input dataset')
flags.DEFINE_integer('metrics_id', 0, 'metric id to predict, 0: attack vector; 1: attack complexity; '
                                      '2: privileges required; 3: user interaction; 4: scope; 5: confidentiality;'
                                      '6: integrity; 7: availability')
flags.DEFINE_boolean('weighted_loss', False, 'whether to use weighted loss')
flags.DEFINE_boolean('slope_anneal', True, 'whether to use slop annealing for attention layer')
flags.DEFINE_boolean('LSTM_embed', True, 'whether to use LSTM embedding')
flags.DEFINE_string('aggregator', 'mean', 'aggregator options')
# hyperparameter for slop anneal
flags.DEFINE_integer('max_slope', 5, 'the max slope for slope annealing')
# hyperparameter for LSTM
flags.DEFINE_float('w', 0.95, 'moving average weight for LSTM embedding')
flags.DEFINE_integer('layers', 2, 'layers for LSTM network')
flags.DEFINE_boolean('raw_seq', False, 'whether to use raw seq or trainable embedding as inputs to LSTM')
flags.DEFINE_integer('embedding_size', 48, 'embedding_szie')
flags.DEFINE_integer('hidden_size', 16, 'hidden size')
flags.DEFINE_integer('attention_size', 1, 'attention size')
flags.DEFINE_integer('num_sentences', 15, 'num of sentences selected for LSTM')
flags.DEFINE_string('pooling', 'mean_pool', 'pooling method for LSTM embedding')
flags.DEFINE_string('model_select', 'accuracy', 'critiria in updating predictions')
# fix bug of flags
flags.FLAGS.__dict__['__parsed'] = False


def main(_):
    config = tf.ConfigProto()
    config.gpu_options.allow_growth = True
    model = GraphNet(tf.Session(config=config), flags.FLAGS)
    model.train()


if __name__ == '__main__':
    # if flags.FLAGS.gpuid:
    #     # configure which gpu or cpu to use
    os.environ['CUDA_VISIBLE_DEVICES'] = '0'
    tf.app.run()
