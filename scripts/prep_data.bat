rem gen_data for 10_days
rem python code/data_prep/gen_raw_features.py --input ./data/10_days/cvss_data.pkl --output ./data/10_days/
rem FOR %%t in (0.3, 0.4) DO (
rem python code/data_prep/gen_inputs.py --end_time 2017-07 --dir ./data/10_days/ --pca_filename pca/10-0-300.pkl --threshold %%t --cat_graph 10-0.pkl
rem python code/data_prep/gen_inputs.py --end_time 2017-08 --dir ./data/10_days/ --pca_filename pca/10-1-300.pkl --threshold %%t --cat_graph 10-1.pkl
rem python code/data_prep/gen_inputs.py --end_time 2017-09 --dir ./data/10_days/ --pca_filename pca/10-2-300.pkl --threshold %%t --cat_graph 10-2.pkl
rem python code/data_prep/gen_inputs.py --end_time 2017-10 --dir ./data/10_days/ --pca_filename pca/10-3-300.pkl --threshold %%t --cat_graph 10-3.pkl
rem python code/data_prep/gen_inputs.py --end_time 2017-11 --dir ./data/10_days/ --pca_filename pca/10-4-300.pkl --threshold %%t --cat_graph 10-4.pkl
rem python code/data_prep/gen_inputs.py --end_time 2017-12 --dir ./data/10_days/ --pca_filename pca/10-5-300.pkl --threshold %%t --cat_graph 10-5.pkl
rem )



python code/gcn/tweet_preprocessing.py --filepath data/10_days/2017-07/
python code/gcn/tweet_preprocessing.py --filepath data/10_days/2017-08/
python code/gcn/tweet_preprocessing.py --filepath data/10_days/2017-09/
python code/gcn/tweet_preprocessing.py --filepath data/10_days/2017-10/
python code/gcn/tweet_preprocessing.py --filepath data/20_days/2017-07/
python code/gcn/tweet_preprocessing.py --filepath data/20_days/2017-08/
python code/gcn/tweet_preprocessing.py --filepath data/20_days/2017-09/
python code/gcn/tweet_preprocessing.py --filepath data/20_days/2017-10/


python code/gcn/tweet_preprocessing.py --filepath data/10_days/2017-11/
python code/gcn/tweet_preprocessing.py --filepath data/10_days/2017-12/

FOR %%t in (0.3) DO (
python code/data_prep/gen_inputs.py --end_time 2017-07 --dir ./data/20_days/ --pca_filename pca/20-0-300.pkl --threshold %%t --cat_graph 20-0.pkl

python code/data_prep/gen_inputs.py --end_time 2017-08 --dir ./data/20_days/ --pca_filename pca/20-1-300.pkl --threshold %%t --cat_graph 20-1.pkl
python code/data_prep/gen_inputs.py --end_time 2017-09 --dir ./data/20_days/ --pca_filename pca/20-2-300.pkl --threshold %%t --cat_graph 20-2.pkl
python code/data_prep/gen_inputs.py --end_time 2017-10 --dir ./data/20_days/ --pca_filename pca/20-3-300.pkl --threshold %%t --cat_graph 20-3.pkl
python code/data_prep/gen_inputs.py --end_time 2017-11 --dir ./data/20_days/ --pca_filename pca/20-4-300.pkl --threshold %%t --cat_graph 20-4.pkl
python code/data_prep/gen_inputs.py --end_time 2017-12 --dir ./data/20_days/ --pca_filename pca/20-5-300.pkl --threshold %%t --cat_graph 20-5.pkl
)
rem python code/gcn/tweet_preprocessing.py --filepath data/20_days/2017-07/
rem python code/gcn/tweet_preprocessing.py --filepath data/20_days/2017-08/
rem python code/gcn/tweet_preprocessing.py --filepath data/20_days/2017-09/
rem python code/gcn/tweet_preprocessing.py --filepath data/20_days/2017-10/
rem python code/gcn/tweet_preprocessing.py --filepath data/20_days/2017-11/
rem python code/gcn/tweet_preprocessing.py --filepath data/20_days/2017-12/


