rem python code/gcn/main.py --max_step 100 --class_num 4 --metrics_id 0 --feat_type basic --what classification --learning rate 0.005 --k 160 --dataset_prefix data/10_days/2017-07/ --metrics_filename ./data/10_days/2017-07/metrics.csv --weighted_loss=True --keep_r 0.5 --adj_keep_r 0.8 --weight_decay 5e-4 --ch_num 2

rem python code/gcn/main.py --max_step 100 --class_num 4 --metrics_id 0 --feat_type basic --what classification --learning rate 0.005 --k 160 --dataset_prefix data/10_days/2017-07/ --metrics_filename ./data/10_days/2017-07/metrics.csv --weighted_loss=True --keep_r 0.5 --adj_keep_r 0.8 --weight_decay 5e-4 --ch_num 2 --LSTM_embed=True

FOR %%t in (True, False) DO (
python code/gcn/main.py --max_step 100 --class_num 4 --metrics_id 0 --feat_type basic --what classification --learning rate 0.005 --k 160 --dataset_prefix data/10_days/2017-07/ --metrics_filename ./data/10_days/2017-07/metrics.csv --weighted_loss=True --keep_r 0.5 --adj_keep_r 0.8 --weight_decay 5e-4 --ch_num 2 --LSTM_embed=%%t

python code/gcn/main.py --max_step 100 --class_num 2 --metrics_id 1 --feat_type basic --what classification --learning rate 0.005 --k 160 --dataset_prefix data/10_days/2017-07/ --metrics_filename ./data/10_days/2017-07/metrics.csv --weighted_loss=True --keep_r 0.5 --adj_keep_r 0.8 --weight_decay 5e-4 --ch_num 2 --LSTM_embed=%%t

python code/gcn/main.py --max_step 100 --class_num 3 --metrics_id 2 --feat_type basic --what classification --learning rate 0.005 --k 160 --dataset_prefix data/10_days/2017-07/ --metrics_filename ./data/10_days/2017-07/metrics.csv --weighted_loss=True --keep_r 0.5 --adj_keep_r 0.8 --weight_decay 5e-4 --ch_num 2 --LSTM_embed=%%t

python code/gcn/main.py --max_step 100 --class_num 2 --metrics_id 3 --feat_type basic --what classification --learning rate 0.005 --k 160 --dataset_prefix data/10_days/2017-07/ --metrics_filename ./data/10_days/2017-07/metrics.csv --weighted_loss=True --keep_r 0.5 --adj_keep_r 0.8 --weight_decay 5e-4 --ch_num 2 --LSTM_embed=%%t

python code/gcn/main.py --max_step 100 --class_num 2 --metrics_id 4 --feat_type basic --what classification --learning rate 0.005 --k 160 --dataset_prefix data/10_days/2017-07/ --metrics_filename ./data/10_days/2017-07/metrics.csv --weighted_loss=True --keep_r 0.5 --adj_keep_r 0.8 --weight_decay 5e-4 --ch_num 2 --LSTM_embed=%%t

python code/gcn/main.py --max_step 100 --class_num 3 --metrics_id 5 --feat_type basic --what classification --learning rate 0.005 --k 160 --dataset_prefix data/10_days/2017-07/ --metrics_filename ./data/10_days/2017-07/metrics.csv --weighted_loss=True --keep_r 0.5 --adj_keep_r 0.8 --weight_decay 5e-4 --ch_num 2 --LSTM_embed=%%t

python code/gcn/main.py --max_step 100 --class_num 3 --metrics_id 6 --feat_type basic --what classification --learning rate 0.005 --k 160 --dataset_prefix data/10_days/2017-07/ --metrics_filename ./data/10_days/2017-07/metrics.csv --weighted_loss=True --keep_r 0.5 --adj_keep_r 0.8 --weight_decay 5e-4 --ch_num 2 --LSTM_embed=%%t

python code/gcn/main.py --max_step 100 --class_num 3 --metrics_id 7 --feat_type basic --what classification --learning rate 0.005 --k 160 --dataset_prefix data/10_days/2017-07/ --metrics_filename ./data/10_days/2017-07/metrics.csv --weighted_loss=True --keep_r 0.5 --adj_keep_r 0.8 --weight_decay 5e-4 --ch_num 2 --LSTM_embed=%%t
)

